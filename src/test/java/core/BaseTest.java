package core;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;



abstract public class BaseTest {//Базовый абстрактный класс(от которого буду наследоваться в других тестовых классах)
    public void setUp() {//настройки для браузера
        WebDriverManager.chromedriver().setup();
        Configuration.browser  = "chrome";
        Configuration.driverManagerEnabled = false;//указываю что присутствует менеджер
        Configuration.browserSize = "1680x1050";
        Configuration.headless = false;//буду ли я видеть сам браузер при выполнении тестов(false -видно)
        Configuration.timeout= 7000;

        //C:\Users\art82\AppData\Local\Google\Chrome\User Data

    }
    public SelenideElement waitElementIsVisible (SelenideElement element){//hhhhhhhhhhhh
        return element.shouldHave(Condition.visible);
    }
    @Before//Метод ДО теста
    public void init(){
        setUp();
    }
    @After//Метод ПОСЛЕ теста
    public void tearDown (){
        Selenide.closeWebDriver();
    }
}



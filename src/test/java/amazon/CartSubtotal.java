package amazon;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class CartSubtotal  {
    public CartPage clickCartButtom(){
        if ($x("//*[@id=\"nav-cart\"]").isDisplayed())
        {
            $x("//*[@id=\"nav-cart\"]").click();
        }
        else {

            $x("//*[@id=\"sc-active-cart\"]/div/div/div/h1").shouldBe(text("\n" + "Shopping Cart"));
            $x("//div[@data-asin=\"B08YKFTZH6\"]").click();
        }
        return new CartPage();
    }
}

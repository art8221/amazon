package amazon;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage {
    public MainPage clickMainManu(){
        $x("//a[@id=\"nav-hamburger-menu\"]").click();
        return this;
    }
    public MainMenuPage waitHmenuItemVisible(){
        $x("//div[@class=\"hmenu-item hmenu-title \"][text()=\"digital content & devices\"]").shouldBe(visible);
        return new MainMenuPage();
    }
}

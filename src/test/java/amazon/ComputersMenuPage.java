package amazon;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ComputersMenuPage {
    public ComputersMenuPage clickComputersTablets(){
        $x("//a[text()=\"Computers & Tablets\"]").click();
        return this;
    }
    public ComputersTabletsPage waitComputersTabletsItemVisible(){
        $x("//*[@class=\"a-size-base a-color-base a-text-bold\"][text()='Computers & Tablets']").shouldBe(visible);
        return new ComputersTabletsPage();
    }

}

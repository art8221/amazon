package amazon;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class LaptopsPage {
    public LaptopsPage searchLaptop(String searchLaptop){
        $x("// input[@id=\"twotabsearchtextbox\"]").setValue(searchLaptop);
        return this;
    }
    public LaptopsPage clickButtom (){
        $x("// input[@id=\"nav-search-submit-button\"]").click();
        return this;
    }
    public LaptopComputers waitSeachResultVisible(){
        $x("//span[text()=\"RESULTS\"]").shouldBe(visible);
        return new LaptopComputers();
    }





}
